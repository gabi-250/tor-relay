{ pkgs, ... }:
let variables = import ./variables.nix;
in
{
  system.stateVersion = "22.11";
  imports = [
    ./hardware-configuration.nix
    ./networking.nix # generated at runtime by nixos-infect
    ./tor-relay.nix
  ];

  environment.systemPackages = with pkgs; [
    binutils
    vim
    fish
    tmux
    htop
    git
    tig

    dnsutils
    lsof
    mtr
    netcat
    tcpdump
    socat
    whois
  ];
  boot.cleanTmpDir = true;
  zramSwap.enable = true;
  networking.hostName = "possum";
  networking.domain = "";
  services.openssh = {
    enable = true;
    ports = [ 2493 ];
  };
  users.users.root = {
    openssh.authorizedKeys.keys = variables.authorizedKeys;
    shell = pkgs.fish;
  };
  users.users.operator = {
    isNormalUser = true;
    home = "/home/operator";
    description = "Relay Operator";
    extraGroups = [ "wheel" "networkmanager" ];
    openssh.authorizedKeys.keys = variables.authorizedKeys;
    shell = pkgs.fish;
  };
}
