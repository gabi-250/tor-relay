{ config, pkgs, ... }:
let variables = import ./variables.nix;
in
{
  environment.systemPackages = with pkgs; [
    tor
    grafana
    prometheus
  ];

  services.tor = {
    enable = true;
    openFirewall = true;
    relay = {
      enable = true;
      role = "relay";
    };
    settings = {
      ContactInfo = variables.services.tor.settings.ContactInfo;
      Nickname = variables.services.tor.settings.Nickname;
      ORPort = [{
        port = 443;
        IPv4Only = false;
      }];
      AccountingStart = "month 1 0:00";
      AccountingMax = "10 TBits";
      #BandwidthRate = "100 KBytes";
      #BandwidthBurst = "500 KBytes";
      ExitRelay = false;
      SocksPort = 0;
      MetricsPort = "127.0.0.1:9001";
      MetricsPortPolicy = "accept 127.0.0.1";
    };
  };

  services.grafana = {
    enable = true;
    settings = {
      server = {
        http_addr = "127.0.0.1";
        http_port = 3000;
      };
    };
    provision = {
      datasources.settings.datasources = [
        {
          name = "prometheus ${config.networking.hostName}";
          type = "prometheus";
          url = "http://127.0.0.1:${toString config.services.prometheus.port}";
        }
      ];
      dashboards.settings.providers = [
        {
          name = "Relay";
          options.path = "/etc/grafana-dashboards";
        }
      ];
      enable = true;
    };
  };

  services.prometheus = {
    enable = true;
    listenAddress = "127.0.0.1";
    port = 9002;
    exporters = {
      node = {
        enable = true;
        enabledCollectors = [ "systemd" ];
        listenAddress = "127.0.0.1";
        port = 9000;
      };
    };

    scrapeConfigs = [
      {
        job_name = "node-exporter";
        static_configs = [{
          targets = [ "127.0.0.1:${toString config.services.prometheus.exporters.node.port}" ];
        }];
      }
      {
        job_name = "tor-relay";
        metrics_path = "/metrics";
        static_configs = [{
          targets = [ "${toString config.services.tor.settings.MetricsPort}" ];
        }];
      }
    ];

  };
}
